#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "xwc.h"
#include "parameters.h"
#include "balas.h"

/*-----------------------------------CONSTANTES------------------------------------------*/

#define massaBala   10      /* Massa da bala, arbitrária */
#define distBoa     1e15      /* Distância da nova bala à nave que a instanciou */
#define vida        20         /* Quantidade de ciclos que a mina vai viver */

/*---------------------------------------------------------------------------------------*/

static cBala * topo;

void initBalas() {
    /* será uma lista ligada com cabeça */
    topo = (cBala *) malloc(sizeof * topo);
    topo->pai = NULL;
    topo->prox = NULL;
}

void novaBala(WINDOW *w1, double navePosX, double navePosY, double naveVelX, double naveVelY, int navePos) {
    cBala * aux;
    double angBala;
    aux = (cBala *) malloc(sizeof * aux);

    angBala = anguloBala(navePos);
    
    /*
    printf("AGULO: %f", angBala*180/3.1415);
    getchar();
    printf("\ncosseno: %f", cos(angBala)*50);
    printf("\nsino: %f", sin(angBala)*50);
    printf("\nPosx nave: %f", navePosX);
    printf("\nPosy nave: %f", navePosY);
    printf("\nPosx calculada: %f", ((long int) navePosX % tela_x) + cos(angBala)*50);
    printf("\nPosy calculada: %f\n", ((long int) navePosY % tela_y) + sin(angBala)*50);
    printf("\nVelx mina: %f", cos(angBala)*3500);
    printf("\nVely mina: %f", sin(angBala)*3500);

    getchar();*/

    aux->pos_x = ((long int) navePosX % tela_x) + cos(angBala)*45; /* 45 é metade da largura da nave */
    aux->pos_y = ((long int) navePosY % tela_y) + sin(angBala)*45*-1;

    if(aux->pos_x > tela_x) aux->pos_x = (long int) aux->pos_x%tela_x;
    else if(aux->pos_x < 0) aux->pos_x = aux->pos_x + tela_x;
    aux->pos_y = ((long int) navePosY % tela_y) + sin(angBala)*45*-1;
    if(aux->pos_y > tela_y) aux->pos_y = (long int) aux->pos_y%tela_y;
    else if(aux->pos_y < 0) aux->pos_y = aux->pos_y + tela_y;

    aux->vel_x = cos(angBala)*3500  ;
    aux->vel_y = sin(angBala)*3500*-1;
    aux->massa = massaBala;
    aux->tempodevida = vida; /* quantidade de ciclos */

    /* parte gráfica */

    aux->mskBala = NewMask(w1, 20, 20);
    aux->dBala = ReadPic(w1, "bala.xpm", aux->mskBala);

    /* atualizando "pai" */
    if(topo->prox != NULL) (topo->prox)->pai = aux;
    aux->pai = topo;

    aux->prox = topo->prox;
    topo->prox = aux;

}

void UnSetMaskBalas() {
    cBala * aux;

    aux = topo->prox;

    while( aux != NULL ) {
        UnSetMask(aux->mskBala);
        aux = aux->prox;
    }
}

void SetMaskBalas(WINDOW *w1) {
    cBala * aux;

    aux = topo->prox;

    while( aux != NULL ) {
        SetMask(w1, aux->mskBala);
        aux = aux->prox;
    }
}

cBala * balasTopo() {
    /*printf("\nDENTRO DA BALASTOPO\nEndereço do topo: %p", (void *) topo->prox);
    */
    return topo->prox;
}

cBala * freeBala(cBala *balinha){
    cBala * proximo;
    cBala * pai;

    proximo = balinha->prox;
    pai = balinha->pai;

    /*printf("\nProximo da balinha: %p", (void *) proximo);*/
    
    if(proximo != NULL) proximo->pai = balinha->pai; 
    free(balinha);
    pai->prox = proximo;

    return proximo;
}

void freeListaBalas() {
    cBala * aux;

    while( topo != NULL ) {
        cBala * aux = topo;
        topo = aux->prox;
        free(aux);
    }

}