#include <stdio.h>
#include <math.h>
#include "/usr/include/X11/keysym.h"
#include "xwc.h"
#include "parameters.h"
#include "balas.h"
#include "keyboard.h"

/****************************************CONSTANTES**************************************************/

#define angleInRad       0.39269908             /* isso é 22.5 graus em radianos */
#define pi 				 3.14159265359
#define propulsao        500                /* arbitrária, posso mudar dps */

/****************************************************************************************************/

void recebeTecla(WINDOW *w1, cNave *nave1, cNave *nave2) { /* dever ser inserido dentro do ciclo! */

    KeySym Foo;
    /* Importante: WGetKey retorna KeyCode em KeySym */
    /* obs: caso queira encontrar o KeySym de dada tecla, rode "xev" no terminal */
	double aux;
    if(WCheckKBD(w1)) {

        WGetKey(w1);
        Foo = WLastKeySym(w1);

        switch(Foo) {
            /* Escolhi esses para a nave 1 */
            /* UP: acelera, LEFT: rotação anti-h,
            * RIGHT: rotação h, ENTER: disparo. */

            case (XK_Left):     /* rotação anti-horária */
            printf("LEFT\n");
            if(nave1->dir < 0.) nave1->dir = 2*pi;
            while(nave1->dir  > 2*pi) nave1->dir = nave1->dir - 2*pi;
            nave1->dir = nave1->dir - angleInRad; 
            if(nave1->dir == 0.) nave1->dir = 2*pi;
            break;
            
            case (XK_Right):    /* rotação horária */
            printf("RIGHT\n");
            if(nave1->dir == 0.) nave1->dir = 2*pi;
            while(nave1->dir  > 2*pi) nave1->dir = nave1->dir - 2*pi;
            nave1->dir = nave1->dir + angleInRad;
            if(nave1->dir == 0.) nave1->dir = 2*pi;
            break;
            
            case (XK_Up):       /* acelera */
            printf("UP\n");
            printf("%f\n", cos(nave1->dir)); /* em radianos */
            printf("%f\n", sin(nave1->dir));
            printf("Direção em rad: %f\n", nave1->dir);
            printf("Direção em ang: %f\n", nave1->dir*180/pi); 
            /* printf("testes para a tela:\n"); 
            if(cos(nave1.dir) > 0 && sin(nave1.dir) > 0) printf("TA NEGATIVO pro y e POS pro x");
            if(cos(nave1.dir) < 0 && sin(nave1.dir) > 0) printf("TA NEGATIVO pro x e Neg pro y");
            if(cos(nave1.dir) > 0 && sin(nave1.dir) < 0) printf("TA NEGATIVO pro x e POS pro y");
            if(cos(nave1.dir) < 0 && sin(nave1.dir) < 0) printf("TA pos pro y e pos pro x");
            printf("\n");*/
            nave1->vel_x += cos(nave1->dir)*propulsao;
            nave1->vel_y += sin(nave1->dir)*propulsao;
            break;
            
            /*
            case (XK_Down):
                printf("DOWN\n");
                break;*/

            case (0xff0d):
                printf("ENTER\n");
                novaBala(w1, nave1->pos_x, nave1->pos_y, nave1->vel_x, nave1->vel_y, nave1->ultimaPos);
                /* preciso ver se consegui */
                break;

                /* Escolhi esses para a nave 2 */
                /* W: acelera, A: rotação anti-h,
                * D: rotação h, S: disparo. */
			case (XK_space):
				printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\\n\n\\n\n\n\\n\n\n\n\n\n\n\n\n\n\nkabum\n");
				if (Poder == 1){
					aux = nave1->pos_x;
					nave1->pos_x = nave2->pos_x;
					nave2->pos_x = aux;
					aux = nave1->pos_y;
					nave1->pos_y = nave2->pos_y;
					nave2->pos_y = aux;
					Poder = 0;
				}
				break;
			case (XK_z):
				printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\\n\n\\n\n\n\\n\n\n\n\n\n\n\n\n\n\nkabum\n");
				if (Poder == 2){
					aux = nave1->pos_x;
					nave1->pos_x = nave2->pos_x;
					nave2->pos_x = aux;
					aux = nave1->pos_y;
					nave1->pos_y = nave2->pos_y;
					nave2->pos_y = aux;
					Poder = 0;
				}
				break;
            case (XK_a):
                printf("LEFT2\n");
                if(nave2->dir < 0.) nave2->dir = 2*pi;
                while(nave2->dir  > 2*pi) nave2->dir = nave2->dir - 2*pi;
                nave2->dir = nave2->dir - angleInRad;
                if(nave2->dir == 0.) nave2->dir = 2*pi;
                break;

            case (XK_d):
                printf("RIGHT2\n");
                if(nave2->dir == 0.) nave2->dir = 2*pi;
                while(nave2->dir  > 2*pi) nave2->dir = nave2->dir - 2*pi;
                nave2->dir = nave2->dir + angleInRad;
                if(nave2->dir == 0.) nave2->dir = 2*pi;
                break;

            case (XK_w):
                printf("UP2\n");
                printf("%f\n", cos(nave2->dir)); /* em radianos */
                printf("%f\n", sin(nave2->dir));
                /*
                if(cos(nave1.dir) > 0 && sin(nave1.dir) > 0) printf("TA NEGATIVO pro y e POS pro x");
                if(cos(nave1.dir) < 0 && sin(nave1.dir) > 0) printf("TA NEGATIVO pro x e Neg pro y");
                if(cos(nave1.dir) > 0 && sin(nave1.dir) < 0) printf("TA NEGATIVO pro x e POS pro y");
                if(cos(nave1.dir) < 0 && sin(nave1.dir) < 0) printf("TA pos pro y e pos pro x");*/
                nave2->vel_x += cos(nave2->dir)*propulsao;
                nave2->vel_y += sin(nave2->dir)*propulsao;
                break;

            case (XK_s):
                printf("DOWN2\n");
                novaBala(w1, nave2->pos_x, nave2->pos_y, nave2->vel_x, nave2->vel_y, nave2->ultimaPos);
                break;
            
            default:
                printf("Ops! O que aconteceu?\n");
                break;
        }
        
    }

}