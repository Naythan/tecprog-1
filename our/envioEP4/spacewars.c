#include <stdio.h>
#include "xwc.h"
#include <unistd.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "parameters.h"
#include "keyboard.h"
#include "balas.h"
#include "/usr/include/X11/keysym.h"
#include "apresentacao.h" /*para a apresentação da tela*/
#include "lexyy.h"

/*-----------------------------------CONSTANTES------------------------------------------*/


#define naninhaDaTela 	 100000 			/* tempo de parada da tela, pode ser alterada */
#define ESCALA			 1	  			/* arbitrária, como então dito no paca */
#define pi 				 3.14159265359		/* importante para pegar a direção */

/*---------------------------------------------------------------------------------------*/


/*-----------------------------------PROTÓTIPOS------------------------------------------*/

/* Recebe um valor double e retorna seu valor absoluto */
double floatAbs (double x);

/* Atualiza os parametros de velocidade, aceleracao e posicao */
void atualizaParametros(cBala *balas, cNave *nave1, cNave *nave2, int numeroBalas, double delta_t, int ciclos, double planetaP);

/* Identifica ocasionais colisões entre corpos */
int bateu (cBala *balas, cNave nave1, cNave nave2, double planetaR, int numeroBalas, int *x, int *y);

/* Calculo necessario para a bateu */
double distancia(double posXi, double posYi, double posXj, double posYj);

/* Exibe imagem img na tela */
void pinta(PIC tela, PIC img, int tam_imgx, int tam_imgy, int posx, int posy);

/* Calcula posições dado o número de ciclos e parâmetros de entrada */
int movimenta(double delta_t, cNave *nave1, cNave *nave2, cBala *balas, int nInt, double planetaP, WINDOW *w1, PIC PicBg,MASK MskBg, cTurbilhao redemoinho);

/* Calcula a direção baseada na velocidade em x e y, retorna ângulo em radianos */
double ObjDir(double vel_x, double vel_y);

/* Recebe velocidade da nave(portanto direção) e retorna imagem na direção correspondente */
PIC picDir(double angulo, cNave nave);

/* Funcao que retorna o indice da mascara/imagem a ser usada para impressao */
int naveDir(double angulo);

/*---------------------------------------------------------------------------------------*/
int Poder;
Poder = 0;

int main(int ac, char **av)
{
	int i;
	int menu;
  	int QuemBateu;
  	int placar[3];
  	int jogo_continua;
	int nInt = 200;		/* Número de ciclos */
	double planetaR; 	/* Define o raio do planeta */
	double planetaP; 	/* Define massa do planeta */
	double tempo; 		/* Define tempo total de simulação */
	double delta_t /*= tempo/nInt*/;
	PIC mina;
	PIC PicBg;			/* Guarda PIC da imagem de fundo */
	MASK MskMina;
	MASK MskBg;			/* Mask da imagem de fundo */
	cNave nave1; 		/* Declaração da (struct) nave1 */
	cNave nave2; 		/* Declaração da (struct) nave2 */
	cTurbilhao redemoinho;
	cBala *balas;
	PIC heartVidas[3];
	MASK maskVidas[3];
	WINDOW *w1;	  		/* Tela */
	PIC PicRet;	/* Para menu final */
	MASK MskRet;/* Para menu final */
	/*cBala *ball;*/
	balas = malloc(sizeof(cBala));
	/*ball = malloc(sizeof(cBala));*/

	nave1.dir = 0;		/* Guarda real direção da nave 1 */
	nave2.dir = 0;		/* Guarda real direção da nave 2 */

	/** Declarações iniciais **/

	planetaR = 5e4;
	planetaP = 6e15;
	tempo = 1.;
	delta_t = tempo/nInt;

	strcpy(nave1.nome, "Sunny");
    nave1.massa = 20;
    nave1.pos_x = -100;
    nave1.pos_y = -100;
    nave1.vel_x = 0;
	nave1.vel_y = 0;

	strcpy(nave2.nome, "Merry");
    nave2.massa = 20;
    nave2.pos_x = 100;
    nave2.pos_y = 100;
    nave2.vel_x = 0;
	nave2.vel_y = 0;



	/* Inicialização da tela */
	w1 = InitGraph(tela_x, tela_y, "Sea Wars");

	
	/**************************/

	/*getParameters(ball, balas, &tempoDaBala, &nave1, &nave2, &tempo, &planetaP, &planetaR, &numeroBalas, &nInt, &delta_t);*/
	/* eu vou retirar a get parameters e vou usar casos arbitrários a partir de agora, para poder 
	 tratar das balas ^*/	

	

  	/* Inicializa leitura do teclado */
  	InitKBD(w1);

	/* Inicialização DA TELA DE APRESENTAÇÃO*/
  	menu = Menu(w1);

  	jogo_continua = 1;

  	placar[0] = 0; 			/*EMPATE*/
  	placar[1] = 0;			/*Vitória da Nave 1*/
  	placar[2] = 0;			/*Vitória da Nave 2*/
  	

	/*EXECUTAR O JOGO*/  
  	while (menu == 1 && jogo_continua == 1) {

		/*nave2.massa = 20;*/
    	nave2.pos_x = 100;
    	nave2.pos_y = 100;
    	nave2.vel_x = 0;
		nave2.vel_y = 0;
		
		/*nave1.massa = 20;*/
    	nave1.pos_x = -100;
    	nave1.pos_y = -100;
    	nave1.vel_x = 0;
		nave1.vel_y = 0;

		/* Inicialização da imagem de fundo */
		MskBg = NewMask(w1, tela_x, tela_y);
		PicBg = ReadPic(w1, "seasea.xpm", MskBg);

		/* Inicialização das naves e suas máscaras */
		MskMina = NewMask(w1, 20, 20);
		mina = ReadPic(w1, "bala.xpm", MskMina);
		desenhosNave(nave1.desenhos, nave1.mascaras, w1, "nave1");
		desenhosNave(nave2.desenhos, nave2.mascaras, w1, "nave2");
		desenhosNave(redemoinho.desenhos, redemoinho.mascaras, w1, "turbilhao");

		redemoinho.pos_x = 0;
		redemoinho.pos_y = 0;
		/* ATENÇÃO! NÃO ALTEREM A ORDEM ABAIXO, ELA É IMPORTANTE
		 * PARA MANTER A TRANSPARÊNCIA FUNCIONANDO, OBRIGADA! 
		 */
		/* Posicionando a imagem de fundo na tela */
		

		PutPic(w1, PicBg, 0, 0, tela_x, tela_y, 0, 0);
		/* Definindo máscaras, necessáriamente nesta ordem: */
		SetMask(w1, MskBg);  													/* Menor prioridade */
		SetMask(w1, redemoinho.mascaras[0]);
		PutPic(w1, redemoinho.desenhos[0], 0, 0, 170, 170, tela_x/2-75, tela_y/2-85);										/* Planeta > Bg */

		/******************************************************/
		initBalas();

		QuemBateu = movimenta(delta_t, &nave1, &nave2, balas, nInt, planetaP, w1, PicBg, MskBg, redemoinho);
		
		if(QuemBateu == 1) {
			placar[0]++; /*As duas naves morreram*/
		} else if (QuemBateu == 3) {
			placar[1]++; /*Nave 1 Venceu, Nave 2 perdeu*/
		} else if (QuemBateu == 2){
			placar[2]++; /*Nave 2 Venceu, Nave 1 perdeu*/
		} else { /*Acabou o TEMPO   empate*/
			placar[0]++;
		}
		for (i = 0; i < 8; i++)
		UnSetMask(redemoinho.mascaras[i]);					/* Mask do Planeta */
		UnSetMask(MskBg);							/* Mask da Bg */
		WClear(w1);

		/* Inicialização do Menu Final*/
		
		MskRet = NewMask(w1, tela_x, tela_y);
		PicRet = ReadPic(w1, "teste.xpm", MskRet);

		SetMask(w1, MskRet);

		jogo_continua = VoceEscolhe(w1, QuemBateu, placar);
	}
	
	freeListaBalas();
	puts("Fim do jogo");
	/* getchar(); */
  	CloseGraph();
  	return 0;
}

/********************************************************
Função movimenta retorna agora a indicação de quem bateu
* 1 -- As duas bateram
* 2 -- Nave 1 bateu
* 3 -- Nave 2 bateu
********************************************************/
int movimenta(double delta_t, cNave *nave1, cNave *nave2, cBala *balas, int nInt, double planetaP, WINDOW *w1, PIC PicBg, MASK MskBg, cTurbilhao redemoinho)
{
	lePega();

	PIC heartVidas[3];
	MASK maskVidas[3];
	PIC indNavio[2];
	MASK indNavioMsk[2];
	MASK ope;
	PIC picOpeOpe;
	PIC explosion;
	MASK mskExplosion;

	cBala * aux;
	int vidasNave1 = vidas;
	int vidasNave2 = vidas;
	int i;
	int ciclo; /* == frame */
	int numeroBalas = 0;
	int QuemBateu;
	int x, y, xx, yy;


	/* Imagens de explosões */
	mskExplosion = NewMask(w1, 100, 100);
	explosion = ReadPic(w1, "exp1.xpm", mskExplosion);

	/* Imagens de representação para navios */
	indNavioMsk[0] = NewMask(w1, 75, 75);
	indNavioMsk[1] = NewMask(w1, 75, 75);
	indNavio[0] = ReadPic(w1, "indmerry.xpm", indNavioMsk[0]);
	indNavio[1] = ReadPic(w1, "indsunny.xpm", indNavioMsk[1]);

	/* Imagem da fruta (poder) */
	ope = NewMask(w1, 50, 50);
	picOpeOpe = ReadPic(w1, "opeope2.xpm", ope);
	
	/* Imagens de representação para vidas */
	for(i = 0; i < 3; i++) maskVidas[i] = NewMask(w1, 75, 25);
	
	heartVidas[0] = ReadPic(w1, "vidas3.xpm", maskVidas[0]);
	heartVidas[1] = ReadPic(w1, "vidas2.xpm", maskVidas[1]);
	heartVidas[2] = ReadPic(w1, "vidas.xpm", maskVidas[2]);
	
	/**************************************
	 *  IMPRIMINDO INFORMAÇÕES DE CONTROLE
	 **************************************/

	printf("\nPOSICAO INICIAL:\n");
	
	printf("Nave 1, posição x: %f posição y: %f\n", (nave1->pos_x), (nave1->pos_y));
	printf("Nave 2, posição x: %f posição y: %f\n", (nave2->pos_x), (nave2->pos_y));

	QuemBateu = bateu(balas, *nave1, *nave2, 50, numeroBalas, &x, &y);
	for(ciclo = 0; ciclo < nInt && (vidasNave2 && vidasNave1) /*QuemBateu <= 0*/; ciclo++)
	{
		/*recebeTecla(w1, &nave1, &nave2);*/
		if(ciclo%100 == 0){
			
			x = rand()%tela_x;
			y = rand()%tela_y;
			xx = (rand())*47 %tela_x; /* multiplicacao por primos */
			yy = (rand())*13 %tela_y; /* multiplicacao por primos */
			
		}

		printf("\nO ciclo e %d\n:", ciclo);
		while(Pendentes())
			recebeTecla(w1, nave1, nave2);

		/***********************************
		 *  LIMPANDO POSICOES NA TELA
		 ***********************************/
		/* 1. Retiro máscaras nesta ordem: */
		for (i = 0; i < 9; i++)
			UnSetMask(redemoinho.mascaras[i]);
			
		UnSetMask(MskBg);		/* Mask da Bg */

		/* 2. Limpo a tela */
		WClear(w1);

		/* 3. Definindo novamente as máscaras, nesta ordem: */
		SetMask(w1, MskBg);
		PutPic(w1, PicBg, 0, 0, tela_x, tela_y, 0, 0);
		SetMask(w1, redemoinho.mascaras[RotacaoRedemoinho(ciclo)]); /* Adicionado*/
		PutPic(w1, redemoinho.desenhos[RotacaoRedemoinho(ciclo)], 0, 0, 170, 170, tela_x/2-75, tela_y/2-85);

		/***********************************************
		 *  ATUALIZACAO DE PARAMETROS DA NOVA POSICAO
		 **********************************************/
		atualizaParametros(balas, nave1, nave2, numeroBalas, delta_t, ciclo, planetaP);
		
		/****************************************
		 *  EXIBICAO DAS POSICOES FINAIS
		 ****************************************/
		/* Essa linha imprime a nave1 na posição x, y da tela */
		nave1->ultimaPos = naveDir(nave1->dir);
		SetMask(w1, nave1->mascaras[nave1->ultimaPos]);
		pinta(w1, nave1->desenhos[nave1->ultimaPos], /*50, 50,*/ 90, 90, nave1->pos_x/ESCALA, nave1->pos_y/ESCALA); 
		UnSetMask(nave1->mascaras[nave1->ultimaPos]);


		/* Essa linha imprime a nave2 na posição x, y da tela */
		nave2->ultimaPos = naveDir(nave2->dir);
		SetMask(w1, nave2->mascaras[nave2->ultimaPos]);
		pinta(w1, nave2->desenhos[nave2->ultimaPos],/*50, 50,*/ 90, 90, nave2->pos_x/ESCALA, nave2->pos_y/ESCALA);
		UnSetMask(nave2->mascaras[nave2->ultimaPos]);

		SetMask(w1, ope);
		pinta(w1, picOpeOpe, 50, 50, x, y);
		UnSetMask(ope);

	
		/* fim criterio para aparecer o monstro */



		/*atualização de vidas: */
		SetMask(w1, indNavioMsk[0]);
		pinta(w1, indNavio[0], 75, 75, 300, 400);
		UnSetMask(indNavioMsk[0]);

		if( vidasNave1 == 3) {
			SetMask(w1, maskVidas[2]);
			pinta(w1, heartVidas[2], 75, 25, 400, 400);
			UnSetMask(maskVidas[2]);
		}
		else if( vidasNave1 == 2 ) {
			SetMask(w1, maskVidas[1]);
			pinta(w1, heartVidas[1], 75, 25, 400, 400);
			UnSetMask(maskVidas[1]);
		}
		else if( vidasNave1 == 1 ) {
			SetMask(w1, maskVidas[0]);
			pinta(w1, heartVidas[0], 75, 25, 400, 400);
			UnSetMask(maskVidas[0]);
		}
		
		SetMask(w1, indNavioMsk[1]);
		pinta(w1, indNavio[1], 75, 75, 600, 400);
		UnSetMask(indNavioMsk[1]);

		if( vidasNave2 == 3 ) {
			SetMask(w1, maskVidas[2]);
			pinta(w1, heartVidas[2], 75, 25, 700, 400);
			UnSetMask(maskVidas[2]);
		}
		else if( vidasNave2 == 2 ) {
			SetMask(w1, maskVidas[1]);
			pinta(w1, heartVidas[1], 75, 25, 700, 400);
			UnSetMask(maskVidas[1]);
		}
		else if( vidasNave2 == 1 ) {
			SetMask(w1, maskVidas[0]);
			pinta(w1, heartVidas[0], 75, 25, 700, 400);
			UnSetMask(maskVidas[0]);
		}

		/* fim atualização de vidas */

		
		SetMaskBalas(w1);
		
		aux = balasTopo();

		while (aux != NULL) {
			/*printf ("\nMINA\nPosx: %f Posy: %f\nEndereço da bala: %p", aux->pos_x/ESCALA, aux->pos_y/ESCALA, (void *) aux);*/
			pinta(w1, aux->dBala, 20, 20, aux->pos_x/ESCALA, aux->pos_y/ESCALA);
			
			aux->tempodevida--;
			if (aux->tempodevida == 0)
			{
				aux = freeBala(aux);
			}
			if (aux != NULL)
				aux = aux->prox;
			/*printf("Auxiliar: %p", (void *) aux);*/
			/*getchar();*/

		}	
		UnSetMaskBalas();
		
		/**************************************
		 *  IMPRIMINDO INFORMAÇÕES DE CONTROLE
	 	**************************************/
		printf("\nPOSICAO Nave1:\n posição x:%f posição y:%f", nave1->pos_x, nave1->pos_y);
		printf("\nPOSICAO Nave2:\n posição x:%f posição y:%f", nave2->pos_x, nave2->pos_y);

		printf("\nvel Nave1:\n vel x:%f vel y:%f", nave1->vel_x, nave1->vel_y);
		printf("\nvel Nave2:\n vel x:%f vel y:%f", nave2->vel_x, nave2->vel_y);
		printf("\nforca Nave1:\n forca x:%f forca y:%f", nave1->forcaRX, nave1->forcaRY);
		printf("\nforca Nave2:\n forca x:%f forcA y:%f", nave2->forcaRX, nave2->forcaRY);
		
		aux = balasTopo();
		while (aux != NULL) {
			printf("\nMINA\nVelocidadex: %g\nVelocidadey: %g\nPosx: %g \nPosy:%g", aux->vel_x, aux->vel_y,aux->pos_x/ESCALA, aux->pos_y/ESCALA);
			aux = aux->prox;
		}	
		
		QuemBateu = bateu(balas, *nave1, *nave2, 50, numeroBalas, &x, &y);
		
		if(QuemBateu == 3) { /* balas */
			SetMask(w1, mskExplosion);
			pinta(w1, explosion, 100, 100, nave2->pos_x, nave2->pos_y);
			UnSetMask(mskExplosion);
			usleep(500);
			vidasNave2--;
			/*inserir as coisas de exp*/
		} /* balas */
		else if(QuemBateu == 2) {
			SetMask(w1, mskExplosion);
			pinta(w1, explosion, 100, 100, nave1->pos_x, nave1->pos_y);
			UnSetMask(mskExplosion);
			usleep(500);
			vidasNave1--;
		}
		else if(QuemBateu == 1) {
			SetMask(w1, mskExplosion);
			pinta(w1, explosion, 100, 100, nave1->pos_x, nave1->pos_y);
			pinta(w1, explosion, 100, 100, nave2->pos_x, nave2->pos_y);
			UnSetMask(mskExplosion);
			usleep(500);
			vidasNave1--;
			vidasNave2--;
		}
		else if(QuemBateu == 5) {
			SetMask(w1, mskExplosion);
			pinta(w1, explosion, 100, 100, nave2->pos_x, nave2->pos_y);
			UnSetMask(mskExplosion);
			usleep(500);
			vidasNave2 = 0;
			/*inserir as coisas de exp*/
		}
		else if(QuemBateu == 4) {
			SetMask(w1, mskExplosion);
			pinta(w1, explosion, 100, 100, nave1->pos_x, nave1->pos_y);
			UnSetMask(mskExplosion);
			usleep(500);
			vidasNave1 = 0;
		}

		/* Descansa pelo tempo da naninha */
		usleep(naninhaDaTela);
	}
	printf("\n");
	if(vidasNave1 > vidasNave2) return 3;      	/* Nave 1 Venceu, Nave 2 perdeu */
	else if(vidasNave2 > vidasNave1) return 2;	/* Nave 2 Venceu, Nave 1 perdeu */
	else return 1;								/* Empate */

	/*return QuemBateu;*/
}

void atualizaParametros(cBala *balas, cNave *nave1, cNave *nave2, int numeroBalas, double delta_t, int ciclo, double planetaP)
{	
	/* PlanetaP eh a massa do planeta */
    cBala * aux;

	nave1->forcaRX = calculaForca(nave1->pos_x, 0, nave1->massa, planetaP);
	nave1->forcaRY = calculaForca(nave1->pos_y, 0, nave1->massa, planetaP);

	/* Força exercida em A pela nave inimiga */
	nave1->forcaRX += calculaForca(nave1->pos_x, nave2->pos_x, nave1->massa, nave2->massa);
	nave1->forcaRY += calculaForca(nave1->pos_y, nave2->pos_y, nave1->massa, nave2->massa);

	/* Nova aceleração da nave 1 */
	nave1->acelRX = calculaAceleracao(nave1->forcaRX, nave1->massa);
	nave1->acelRY = calculaAceleracao(nave1->forcaRY, nave1->massa);

	/* Nova velocidade da nave 1 */
	nave1->vel_x = calculaVel(nave1->acelRX, nave1->vel_x, delta_t, ciclo*delta_t);
	nave1->vel_y = calculaVel(nave1->acelRY, nave1->vel_y, delta_t, ciclo*delta_t);
	/* Limitando a velocidade à 10000. (è bastante, recomendo limitar até 5k)*/
	if (nave1->vel_x > 0 && nave1->vel_x > 10000)
		nave1->vel_x = 10000;
	if (nave1->vel_x < 0 && nave1->vel_x < -10000)
		nave1->vel_x = -10000;
	if (nave1->vel_y > 0 && nave1->vel_y > 10000)
		nave1->vel_y = 10000;
	if (nave1->vel_y < 0 && nave1->vel_y < -10000)
		nave1->vel_y = -10000;

		
	/* NAVE 2   NAVE 2   NAVE 2   NAVE 2   NAVE 2   NAVE 2 */

	/* Força exercida em B pelo planeta*/
	nave2->forcaRX = calculaForca(nave2->pos_x, 0, nave2->massa, planetaP);
	nave2->forcaRY = calculaForca(nave2->pos_y, 0, nave2->massa, planetaP);

	/* Força exercida em B pela nave A*/
	nave2->forcaRX += calculaForca(nave2->pos_x, nave1->pos_x, nave2->massa, nave1->massa);
	nave2->forcaRY += calculaForca(nave2->pos_y, nave1->pos_y, nave2->massa, nave1->massa);

	/*Nova aceleração da nave 2 */
	nave2->acelRX = calculaAceleracao(nave2->forcaRX, nave2->massa);
	nave2->acelRY = calculaAceleracao(nave2->forcaRY, nave2->massa);
		
	/*Nova velocidade da nave 2 */
	nave2->vel_x = calculaVel(nave2->acelRX, nave2->vel_x, delta_t, ciclo*delta_t);
	nave2->vel_y = calculaVel(nave2->acelRY, nave2->vel_y, delta_t, ciclo*delta_t);

	/* Limitando a velocidade à 10000. (è bastante, recomendo limitar até 5k)*/
	if (nave2->vel_x > 0 && nave2->vel_x > 5000)
		nave2->vel_x = 5000;
	if (nave2->vel_x < 0 && nave2->vel_x < -5000)
		nave2->vel_x = -5000;
	if (nave2->vel_y > 0 && nave2->vel_y > 5000)
		nave2->vel_y = 5000;
	if (nave2->vel_y < 0 && nave2->vel_y < -5000)
		nave2->vel_y = -5000;

	if(ciclo == 0 )
	{
		nave1->dir = ObjDir(nave1->vel_x, nave1->vel_y);
		nave2->dir = ObjDir(nave2->vel_x, nave2->vel_y);
		while(nave2->dir < 0) nave2->dir = 2*pi + nave2->dir;
		while(nave1->dir < 0) nave1->dir = 2*pi + nave1->dir;
	}

	printf("DIREÇÃO DA NAVE 1: %f\n", nave1->dir);
	printf("DIREÇÃO DA NAVE 2: %f\n", nave2->dir);

	/***************************************
	 *    CALCULO DAS POSICOES FINAIS
	 ***************************************/

	aux = balasTopo();
	while ( aux != NULL ) {
		aux->pos_x = calculaPos(aux->pos_x, aux->vel_x, aux->acelRX, delta_t, ciclo*delta_t);
		aux->pos_y = calculaPos(aux->pos_y, aux->vel_y, aux->acelRY, delta_t, ciclo*delta_t);
		aux = aux->prox;
	} 
	
	nave1->pos_x = calculaPos(nave1->pos_x, nave1->vel_x, nave1->acelRX, delta_t, ciclo*delta_t);
	nave1->pos_y = calculaPos(nave1->pos_y, nave1->vel_y, nave1->acelRY, delta_t, ciclo*delta_t);

	nave2->pos_x = calculaPos(nave2->pos_x, nave2->vel_x, nave2->acelRX, delta_t, ciclo*delta_t);
	nave2->pos_y = calculaPos(nave2->pos_y, nave2->vel_y, nave2->acelRY, delta_t, ciclo*delta_t);

	/*  AJUSTANDO A POSIÇÃO DAS NAVES PARA FICAREM DENTRO DA TELA */

	/* NAVE 1 */
	if (nave1->pos_x > tela_x)
		nave1->pos_x = (long int) nave1->pos_x % tela_x;
	else if (nave1->pos_x < 0)
		nave1->pos_x = nave1->pos_x + tela_x;

	if (nave1->pos_y > tela_y)
		nave1->pos_y = (long int) nave1->pos_y % tela_y;
	else if (nave1->pos_y < 0)
		nave1->pos_y = nave1->pos_y + tela_y;


	/* NAVE 2 */
	if (nave2->pos_x > tela_x)
		nave2->pos_x = (long int) nave2->pos_x % tela_x;
	else if (nave2->pos_x < 0)
		nave2->pos_x = nave2->pos_x + tela_x;

	if (nave2->pos_y > tela_y)
		nave2->pos_y = (long int) nave2->pos_y % tela_y;
	else if (nave2->pos_y < 0)
		nave2->pos_y = nave2->pos_y + tela_y;
	
	/* BALAS */
	aux = balasTopo();
	while ( aux != NULL ) {
		
		if(aux->pos_x > tela_x) aux->pos_x = (long int) aux->pos_x%tela_x;
		else if(aux->pos_x < 0) aux->pos_x = aux->pos_x + tela_x;

		if(aux->pos_y > tela_y) aux->pos_y = (long int) aux->pos_y%tela_y;
		else if(aux->pos_y < 0) aux->pos_y = aux->pos_y + tela_y;

		aux = aux->prox;
	}
}

int bateu (cBala *balas, cNave nave1, cNave nave2, double planetaR, int numeroBalas, int *x, int *y)
{
	int batida;
	cBala *aux = balasTopo(); /* balas topo() ?*/
	
	batida = 0;
	
	/*Nave1 com Nave2*/
	if(distancia(nave1.pos_x/ESCALA, nave1.pos_y/ESCALA, nave2.pos_x/ESCALA, nave2.pos_y/ESCALA) < 70) {
		batida = 1;
	}	
	
	while( aux!= NULL && !batida ) { 
	
		/* Nave 1 com bala */
	
		if(distancia(nave1.pos_x/ESCALA, nave1.pos_y/ESCALA, aux->pos_x, aux->pos_y) < 35) {
			batida = 2;
			aux = freeBala(aux);
			break;
		}
		/* Nave 2 com bala */
	
		if(distancia(nave2.pos_x/ESCALA, nave2.pos_y/ESCALA, aux->pos_x, aux->pos_y) < 35) {
			batida = 3;
			aux = freeBala(aux);
			break;
		}

		aux = aux->prox;
	}
	
	/* Nave 1 com planeta */
	if(distancia(nave1.pos_x/ESCALA, nave1.pos_y/ESCALA, 0, 0) < 105) {
		batida = 4;
	}
	
	/* Nave 2 com planeta */
	if(distancia(nave2.pos_x/ESCALA, nave2.pos_y/ESCALA, 0, 0) < 105) {
		batida = 5;
	}

	/* ENCONTROU FRUTA! */

	if (distancia(nave2.pos_x/ESCALA, nave2.pos_y/ESCALA, *x, *y) < 70){
		Poder = 1;
		*x = 0;
		*y = 0;
	}
	if (distancia(nave1.pos_x/ESCALA, nave1.pos_y/ESCALA, *x, *y) < 70){
		Poder = 2;
		*x = 0;
		*y = 0;
	}

	return batida;
}

double distancia(double posXi, double posYi, double posXj, double posYj)
{
	return	(sqrt(( posXi - posXj)*( posXi - posXj) +
				  ( posYi - posYj)*( posYi - posYj)));
}

double floatAbs (double x){
	if (x < 0.)
		return -x;
	return x;
}

void pinta(PIC tela, PIC img, int tam_imgx, int tam_imgy, int posx, int posy)
{
	/* calculando x e y  na tela */
	int x = ((posx + tela_x/2) % tela_x)- tam_imgx/2;
	int y = ((posy + tela_y/2) % tela_y)- tam_imgy/2;

	/*printf("\no x e %d e o y e %d\n", x, y);*/

	PutPic(tela, img, 0, 0, tam_imgx, tam_imgy, x, y);
}


double ObjDir(double vel_x, double vel_y) {

	return(atan2(vel_y, vel_x));
	
	/* obs: o ângulo é dado por...
	 *        270      
	 *   180       0
	 *        90      
	 * 
	 */
}

PIC picDir(double angulo, cNave nave) {
	double angle;

	/* tenho que mudar dps as imagens, talvez */

	/* ângulo em graus */
	angle = angulo*180/pi;
	
	if(0 < angle && angle <= 22.5) {
		printf("%f\n", angle);
		return nave.desenhos[4];
	}

	else if(22.5 < angle && angle <= 45) {
		printf("%f\n", angle);
		/* getchar(); */
		return nave.desenhos[5];
	}

	else if(45 < angle && angle <= 67.5) {
		printf("%f\n", angle);
		/* getchar(); */
		return nave.desenhos[6];
	}

	else if(67.5 < angle && angle <= 90) {
		printf("%f\n", angle);
		return nave.desenhos[7];
	}

	else if(90 < angle && angle <= 112.5) {
		printf("%f\n", angle);
		return nave.desenhos[9];
	}

	else if(112.5 < angle && angle <= 135) {
		printf("%f\n", angle);
		return nave.desenhos[10];
	}

	else if(135 < angle && angle <= 157.5) {
		printf("%f\n", angle);
		return nave.desenhos[11];
	}

	else if(157.5 < angle && angle <= 180) {
		printf("%f\n", angle);
		return nave.desenhos[12];
	}

	else if(180 < angle && angle <= 202.5) {
		printf("%f\n", angle);
		return nave.desenhos[13];
	}

	else if(202.5 < angle && angle <= 225) {
		printf("%f\n", angle);
		/* getchar(); */
		return nave.desenhos[14];
	}

	else if(225 < angle && angle <= 247.5) {
		printf("%f\n", angle);
		/* getchar(); */
		return nave.desenhos[15];
	}

	else if(247.5 < angle && angle <= 270) {
		printf("%f\n", angle);
		/* getchar(); */
		return nave.desenhos[0];
	}

	else if(270 < angle && angle <= 292.5) {
		printf("%f\n", angle);
		/* getchar(); */
		return nave.desenhos[1];
	}

	else if(292.5 < angle && angle <= 315) {
		printf("%f\n", angle);
		/* getchar(); */
		return nave.desenhos[2];
	}

	else if(315 < angle && angle <= 337.5) {
		printf("%f\n", angle);
		/* getchar(); */
		return nave.desenhos[3];
	}
	else {
		printf("%f\n", angle);
		/* getchar(); */
		return nave.desenhos[4];
	}
}

int naveDir(double angulo) {
	double angle;

	/* ângulo em graus */
	angle = angulo*180/pi;

	if(0 < angle && angle <= 22.5) {
		return 4;
	}

	else if(22.5 < angle && angle <= 45) {
		return 5;
	}

	else if(45 < angle && angle <= 67.5) {
		return 6;
	}

	else if(67.5 < angle && angle <= 90) {
		return 7;
	}

	else if(90 < angle && angle <= 112.5) {
		return 9;
	}

	else if(112.5 < angle && angle <= 135) {
		return 10;
	}

	else if(135 < angle && angle <= 157.5) {
		return 11;
	}

	else if(157.5 < angle && angle <= 180) {
		return 12;
	}

	else if(180 < angle && angle <= 202.5) {
		return 13;
	}

	else if(202.5 < angle && angle <= 225) {
		return 14;
	}

	else if(225 < angle && angle <= 247.5) {
		return 15;
	}

	else if(247.5 < angle && angle <= 270) {
	
		return 0;
	}

	else if(270 < angle && angle <= 292.5) {
		return 1;
	}

	else if(292.5 < angle && angle <= 315) {
		return 2;
	}

	else if(315 < angle && angle <= 337.5) {
		return 3;
	}

	else {
		return 4;
	}
}

