#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "/usr/include/X11/keysym.h"
#include "xwc.h"
#include "parameters.h"
#include "balas.h"
#include "keyboard.h"

#define tela_x 			 1100			  	/* dimensão x da tela */
#define tela_y 			 700			  	/* dimensão y da tela */
#define MAX 			 128
#define centro_x  		 450
#define centro_y


int Apresentacao (WINDOW *w1) {
	/* Inicialização DA TELA DE APRESENTAÇÃO*/
	Color c;
	PIC bgInicio = ReadPic(w1, "BGSUNNY.xpm", NULL);

	/*WCor(w1,c=WNamedColor("gold"));*/
	/*WFillRect(w1, 0, 0, tela_x, tela_y, c);*/
	PutPic(w1, bgInicio, 0, 0, tela_x, tela_y, 0, 0);

	/*
	WCor(w1,c=WNamedColor("black"));
	WPrint(w1, centro_x, 50, "SPACEWARS GAME");*/

	WCor(w1,c=WNamedColor("black"));
	WPrint(w1, 200, 550, "Escolha a opcao e digite ENTER");

    KeySym abertura;
    abertura = XK_Left; 		/*variavel da tecla*/
    int posicao = 0;			/*Variavel que contabiliza a posição*/

    /*****LOOP QUE MANTÉM A APRESENTAÇÃO******/
	while (abertura != 0xff0d){
		/*Se  tecla pra cima:*/
    	if (abertura == XK_Up || abertura == XK_w) {
    		if( posicao - 1 < 0) {
    			posicao = 2;
    		} else {
    			posicao = (posicao-1)%3;
    		}
    	} 
    	/*Se tecla pra baixo*/
    	else if (abertura == XK_Down || abertura ==  XK_s) {
			posicao = (posicao+1)%3;
    	}

    	/*Indicação visual para o usuário*/
    	if (posicao == 0) {
    		/*Indica o INICIAR*/
			WCor(w1,c=WNamedColor("red"));
			WPrint(w1, /*centro_x+10*/200, 250, "INICIAR");
			WCor(w1,c=WNamedColor("blue"));
			WPrint(w1, /*centro_x+12*/202, 350, "AJUDA");
			WCor(w1,c=WNamedColor("blue"));
			WPrint(w1, /*centro_x+15*/ 205, 450, "SAIR");
    	} else if (posicao == 1) {
    		/*indica o AJUDA*/
			WCor(w1,c=WNamedColor("blue"));
			WPrint(w1, 200, 250, "INICIAR");
			WCor(w1,c=WNamedColor("red"));
			WPrint(w1, 202, 350, "AJUDA");
			WCor(w1,c=WNamedColor("blue"));
			WPrint(w1, 205, 450, "SAIR");
    	} else {
    		/*indica o SAIR*/
			WCor(w1,c=WNamedColor("blue"));
			WPrint(w1, 200, 250, "INICIAR");
			WCor(w1,c=WNamedColor("blue"));
			WPrint(w1, 202, 350, "AJUDA");
			WCor(w1,c=WNamedColor("red"));
			WPrint(w1, 205, 450, "SAIR");
    	}
		WGetKey(w1);
		abertura = WLastKeySym(w1);
	}
	return (posicao+1);
}

int Menu(WINDOW *w1) {
	/* Inicialização DA TELA DE APRESENTAÇÃO*/
	Color c;
	WCor(w1,c=WNamedColor("gold"));

	WFillRect(w1, 0, 0, tela_x, tela_y, c);

    KeySym abertura;
    abertura = XK_Left; 		/*variavel da tecla*/

  	int menu;
  	menu = Apresentacao(w1);
  	/***************Loop sobre as Regras****************/
  	while (menu == 2) {
  		/*Mostre as regras e etc.*/
		WCor(w1,c=WNamedColor("gold"));
		WFillRect(w1, 0, 0, tela_x, tela_y, c);
		WCor(w1,c=WNamedColor("black"));
		WPrint(w1, 450, 50, "SPACEWARS GAME -- AJUDA");
		WCor(w1,c=WNamedColor("red"));
		WPrint(w1, 350, 100, "*********************************************************");
		WPrint(w1, 350, 115, "                        COMO JOGAR?"); 
		WPrint(w1, 350, 130, "+ Existem dois navios: ");
		WPrint(w1, 350, 145, "    Player 1 eh o Merry (navio ovelha);");
		WPrint(w1, 350, 160, "    Player 2 eh o Sunny (navio leao).");
		WPrint(w1, 350, 190, "+ CONTROLES:");
		WPrint(w1, 350, 205, "[Temos a opcao de acelerar, mudar a direcao (horario ou anti-horario), e atirar balas]");
		WPrint(w1, 350, 220, "    +Player 1 (Merry): "); 
		WPrint(w1, 350, 235, "    Rotacao horaria: botao direito;");
		WPrint(w1, 350, 250, "    Rotaçao anti-horaria: botao esquerdo;");
		WPrint(w1, 350, 265, "    Aceleracao: botao para cima;");
		WPrint(w1, 350, 280, "    Tiros: enter;");
		WPrint(w1, 350, 295, "    Rotacao horaria: D;"); 
		WPrint(w1, 350, 310, "    Rotacao anti-horaria: A;");
		WPrint(w1, 350, 325, "    Aceleracao: W;");
		WPrint(w1, 350, 340, "    Tiros: S;");
		WPrint(w1, 350, 370, "+ PODERES ESPECIAIS:"); 
		WPrint(w1, 350, 385, "Caso recolha uma fruta, voce pode trocar de lugar com o outro navio!");
		WPrint(w1, 350, 400, "Aperte espaço para trocar de lugar.");
		WCor(w1,c=WNamedColor("blue"));
		WPrint(w1, 350, 500, "Aperte ENTER para voltar.");
		WGetKey(w1);
		abertura = WLastKeySym(w1);
		if (abertura == 0xff0d) menu = Apresentacao(w1);

  	}
  	return menu;
}

int VoceEscolhe(WINDOW *w1, int batida, int *placar) {
	Color c;
    KeySym bora;
    bora = XK_Up; 		/*variavel da tecla*/
    int posicao;
    posicao = 1;
    char texto[MAX];

    /*****LOOP QUE MANTÉM A APRESENTAÇÃO******/
	while (bora != 0xff0d){
		/*Se  tecla pra direita:*/
    	if (bora == XK_Right || bora == XK_d) {
    		posicao++;
    	} 
    	/*Se tecla pra baixo*/
    	else if (bora == XK_Left || bora ==  XK_a) {
			posicao--;
    	}
		if (batida == 1) {
			WCor(w1,c=WNamedColor("gold"));
			WFillRect(w1, 0, 0, tela_x, tela_y, c);
			WCor(w1,c=WNamedColor("black"));
			WPrint(w1, centro_x, 50, "As Duas Naves Morreram");

		} else if (batida == 2) {
			WCor(w1,c=WNamedColor("gold"));
			WFillRect(w1, 0, 0, tela_x, tela_y, c);
			WCor(w1,c=WNamedColor("black"));
			WPrint(w1, centro_x, 50, "A Nave 2 Venceu");

		} else if (batida == 3) {
			WCor(w1,c=WNamedColor("gold"));
			WFillRect(w1, 0, 0, tela_x, tela_y, c);
			WCor(w1,c=WNamedColor("black"));
			WPrint(w1, centro_x, 50, "A Nave 1 Venceu");

		} else { 
			WCor(w1,c=WNamedColor("gold"));
			WFillRect(w1, 0, 0, tela_x, tela_y, c);
			WCor(w1,c=WNamedColor("black"));
			WPrint(w1, centro_x+10, 50, "FIM DO TEMPO");
		}
		WCor(w1,c=WNamedColor("black"));
		WPrint(w1, centro_x+30, 150, "PLACAR");

		snprintf(texto, MAX*sizeof(char), "%d", placar[1]);
		WPrint(w1, centro_x-20, 200, "MERRY");
		WPrint(w1, centro_x-10, 230, texto);
		WLine(w1, centro_x+20, 190, centro_x+20, 230, c);

		snprintf(texto, MAX*sizeof(char), "%d", placar[0]);
		WPrint(w1, centro_x+30, 200, "EMPATE");
		WPrint(w1, centro_x+45, 230, texto);
		WLine(w1, centro_x+70, 190, centro_x+70, 230, c);

		snprintf(texto, MAX*sizeof(char), "%d", placar[2]);
		WPrint(w1, centro_x+80, 200, "SUNNY");
		WPrint(w1, centro_x+80, 230, texto);

		WPrint(w1, centro_x-10, 350, "Quer continuar a jogar?");

    	if (posicao%2 != 0) {
			WCor(w1,c=WNamedColor("red"));
			WPrint(w1, centro_x+10, 400, "SIM");

			WCor(w1,c=WNamedColor("black"));
			WPrint(w1, centro_x+60, 400, "NAO");
    	} else {

			WCor(w1,c=WNamedColor("black"));
			WPrint(w1, centro_x+10, 400, "SIM");

			WCor(w1,c=WNamedColor("red"));
			WPrint(w1, centro_x+60, 400, "NAO");
    	}

		WGetKey(w1);
		bora = WLastKeySym(w1);
	}
	/*Se indicou o NÃO*/
	if (posicao%2 == 0 ) {
		return 0;
	}
	/*Se indicou o Sim*/ 
	else {
		return 1;
	}
}