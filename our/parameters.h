/**********************************************************************
	MAC0216 - Tecnicas de Programação
	
	Equipe: 
		Daniel Angelo Esteves Lawand
		Lara Ayumi Nagamatsu
		Lucy Anne de Omena Evangelista
		Nathan de Oliveira Nunes

***********************************************************************/
extern int Poder;
#define MAX 100
#define tela_x 			 1100			  	/* dimensão x da tela */
#define tela_y 			 700			  	/* dimensão y da tela */

typedef struct {
    char nome[MAX];
    int ultimaPos;
    double massa;
    double pos_x, pos_y;
    double vel_x, vel_y; 
    double forcaRX, forcaRY;
    double acelRX, acelRY;
    double dir;             /* direção da nave */
    PIC desenhos[16];
    MASK mascaras[16];
    PIC imgcorrente;
} cNave;

typedef struct balinhas {
    double massa;
    double pos_x, pos_y;
    double vel_x, vel_y;
    double forcaRX, forcaRY;
    double acelRX, acelRY;
    int tempodevida;
    PIC dBala;
    MASK mskBala;
    struct balinhas *prox;
    struct balinhas *pai;
} cBala;


typedef struct monsteers {
    PIC desenhos[3];
    MASK mascaras[3];
    double pos_x, pos_y;
    double massa;
    double raio;
    int ativo;
    int vida;
} cMonstro;
/* Adicionado */
typedef struct turbilhao {
    PIC desenhos[9];
    MASK mascaras[9];
    double pos_x, pos_y;
} cTurbilhao;
/* fim adicionado*/

double calculaAceleracao(double totalForca, double massa);

double calculaForca(double pos_a, double pos_b, double massa_a, double massa_b);

double calculaVel(double acel, double vel_0, double passo, double tempototal);

double calculaPos(double pos_0, double vel_0, double acel, double passo, double tempototal);

void getParameters(cBala *ball, cBala *balas, double *tempoDaBala, cNave *nave1, cNave *nave2, double *tempo, double *planetaP, double *planetaR, int *numeroBalas, int *nInt, double *delta_t);

void desenhosNave(PIC *des, MASK *masc, WINDOW *w1, char nave[5]);

int RotacaoRedemoinho (int nCiclos);

double anguloBala(int posNave);

/*
PIC orientacao(double vel_x, double vel_y, PIC des[16]);*/