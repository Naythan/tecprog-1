/**********************************************************************
	MAC0216 - Tecnicas de Programação
	Primeira fase - Simulação das Órbitas
	
	Equipe: 
		Daniel Angelo Esteves Lawand
		Lara Ayumi Nagamatsu
		Lucy Anne de Omena
		Nathan de Oliveira Nunes

***********************************************************************/
#include <stdio.h>
#include <stdlib.h>

#define MAX 100

typedef struct {
    char nome[MAX];
    double massa;
    double pos_x, pos_y;
    double vel_x, vel_y; 
} cNave;

typedef struct {
    double massa;
    double pos_x, pos_y;
    double vel_x, vel_y;
} cBala;

 
/******************************************************
 * double calculaAceleracao()
 * Na função abaixo, o interesse é de calcular as forças 
 * em qualquer eixo sobre o corpo A a partir do corpo B
 ******************************************************/
double calculaForca(double pos_a, double pos_b, double massa_a, double massa_b) {
  
    double forca, delta;
    double G = 0.00000000067;

    /* delta é a distância de pos_a a pos_b */
	delta = pos_a - pos_b;
    if(delta < 0) delta = -delta;

    /* caso numa distância muito pequena, não exercem forças */
    if(delta < 0.5) forca = 0;
    else forca = (G * massa_a * massa_b)/(delta*delta);

    /* 
     * Caso o corpo A esteja à direita do corpo B no eixo X ou
     * caso o corpo A esteja acima do corpo B no eixo Y, a força
     * que B exerce em A "puxa" o corpo A para a esquerda, ou o
     * "puxa" para baixo, de qualquer forma a força é negativa.
     */
    if(pos_a > pos_b) forca = -forca;

    return forca;
}


 /******************************************************
  * double calculaAceleracao()
  *	Função para cálculo da aceleração com base na força
  * e massa do objeto.
  *
 ******************************************************/
double calculaAceleracao(double totalForca, double massa) {

    return (totalForca/massa);
}

/******************************************************
 * double calculaVel()
 *	Função para cálculo da aproximação da velocidade a partir do
 *	Método de Runge Kutta de segunda ordem (Método de Heun)
 *
 * link de referência:/ http://conteudo.icmc.usp.br/pessoas/andretta/ensino/aulas/sme0300-2-13-fisica/aula20-edorungekutta.pdf
 ******************************************************/
double calculaVel(double acel, double vel_0, double passo, double tempototal) {
	double k1, k2, runge;
	
	k1 = vel_0 + acel * tempototal;
    k2 = (vel_0 + passo * k1 * 2./3.) + acel * (tempototal + passo * 2./3.);
    
    runge = vel_0 + (passo/4.) * (k1 + 3*k2);	

    return runge;
}


 /******************************************************
  * double calculoPos()
  *	Função para cálculo da aproximação da velocidade a partir do
  *	Método de Runge Kutta de segunda ordem (Método de Heun)
  *
  * link de referência:/ http://conteudo.icmc.usp.br/pessoas/andretta/ensino/aulas/sme0300-2-13-fisica/aula20-edorungekutta.pdf
 ******************************************************/
double calculaPos(double pos_0, double vel_0, double acel, double passo, double tempototal) {
    double pos, k1, k2;
    
    k1 = pos_0 + vel_0 * tempototal;
    k2 = (pos_0 + passo * k1 * 2./3.) + vel_0 * (tempototal + passo * 2./3.);
    
    pos = vel_0 + (passo/4.) * (k1 + 3*k2);

    return pos;
} 

/* **********************************************************
 * void getParameters()
 * Função para captação dos parâmetros e exibição de simulação.
 * Por conta do parâmetro da quantidade de balas mudar e assim,
 * consequentemente, as especificações de cada bala poderem ser
 * diferentes, o cálculo das forças exercidas sobre os corpos
 * ocorre na função getParameters.
 ************************************************************/
void getParameters(){
    char nomeArquivo[MAX] = "";
    int i, j;
    int ciclo;
    double planetaR; /* define o raio do planeta */
    double planetaP; /* define massa do planeta */
    double tempo; /* define tempo total de simulação */
    cNave nave1;
    cNave nave2;
    int numeroBalas;
    double tempoDaBala;
    double delta_t;
    int nInt = 3;
    double forcaRX;
    double forcaRY;
    double acelRX;
    double acelRY;
    cBala *balas;
    FILE *entrada;
    
    printf("%s", "Digite o nome do arquivo de onde carregar os parametros: ");
    scanf("%s", nomeArquivo);
    entrada = fopen(nomeArquivo, "r");
    
	if (entrada == NULL) 
	{
      printf ("\nNão encontrei o arquivo!\n");
      exit (EXIT_FAILURE);
    }

    /*Primeira Linha do Arquivo: Definições Planetas*/
    fscanf(entrada, "%lf ", &planetaR); 
    fscanf(entrada, "%lf ", &planetaP);
    fscanf(entrada, "%lf ", &tempo); 

    delta_t = tempo/nInt;

    /*Segunda Linha do Arquivo: nave 1*/
    fscanf(entrada, "%s ", nave1.nome);
    fscanf(entrada, "%lf ", &nave1.massa);
    fscanf(entrada, "%lf ", &nave1.pos_x);
    fscanf(entrada, "%lf ", &nave1.pos_y);
    fscanf(entrada, "%lf ", &nave1.vel_x);
    fscanf(entrada, "%lf ", &nave1.vel_y);

    /*Terceira Linha do Arquivo: nave 2*/
    fscanf(entrada, "%s ", nave2.nome);
    fscanf(entrada, "%lf ", &nave2.massa);
    fscanf(entrada, "%lf ", &nave2.pos_x);
    fscanf(entrada, "%lf ", &nave2.pos_y);
    fscanf(entrada, "%lf ", &nave2.vel_x);
    fscanf(entrada, "%lf ", &nave2.vel_y);

    /*Quarta Linha do Arquivo: numero e tempo das Balas*/   
    fscanf(entrada, "%d ", &numeroBalas);
    fscanf(entrada, "%lf ", &tempoDaBala);

    /*Declarando cada bala*/
    
    balas = malloc(numeroBalas * sizeof(cBala));

    /*5+i Linha do Arquivo: iésima bala*/
    for (i = 0; i < numeroBalas; i++) 
	{
	    fscanf(entrada, "%lf ", &balas[i].massa);
	    fscanf(entrada, "%lf ", &balas[i].pos_x);
	    fscanf(entrada, "%lf ", &balas[i].pos_y);
	    fscanf(entrada, "%lf ", &balas[i].vel_x);
	    fscanf(entrada, "%lf ", &balas[i].vel_y);
    }

    printf("\nplaneta Raio:  %e\n", planetaR);
    printf("planeta Peso: %e\n", planetaP);
    printf("tempo: %e\n\n", tempo);

    printf("Nome nave1: %s\n", nave1.nome);
    printf("Massa nave1: %e\n", nave1.massa);
    printf("Posição x: %e\n", nave1.pos_x);
    printf("Posição y: %e\n", nave1.pos_y);
    printf("Velocidade x: %e\n", nave1.vel_x);
    printf("Velocidade y: %e\n\n", nave1.vel_y);

    printf("Nome nave2: %s\n", nave2.nome);
    printf("Massa nave2: %e\n", nave2.massa);
    printf("Posição x: %e\n", nave2.pos_x);
    printf("Posição y: %e\n", nave2.pos_y);
    printf("Velocidade x: %e\n", nave2.vel_x);
    printf("Velocidade y: %e\n\n", nave2.vel_y);

    printf("Numero Balas: %d\n", numeroBalas);
    printf("Tempo da Bala: %e\n\n", tempoDaBala);

    for (i = 0; i < numeroBalas; i++) 
	{
	    printf("Massa Bala[%d]: %e\n", i+1, balas[i].massa);
	    printf("Vel x Bala[%d]: %e\n", i+1, balas[i].vel_x);
	    printf("Vel y Bala[%d]: %e\n\n", i+1, balas[i].vel_y);
	    printf("Pos x Bala[%d]: %e\n", i+1, balas[i].pos_x);
	    printf("Pos y Bala[%d]: %e\n", i+1, balas[i].pos_y);

    }
 
    for(ciclo = 0; ciclo < nInt; ciclo++) {
        printf("\n%d CICLO", ciclo+1);
        printf("\n--------------------------------------------------------------");
        printf("\nNAVE 1: ESPECIFICAÇÕES\n");
        /* força exercida em A pelo planeta*/
        forcaRX = calculaForca(nave1.pos_x, 0, nave1.massa, planetaP);
        forcaRY = calculaForca(nave1.pos_y, 0, nave1.massa, planetaP);

        /* força exercida em A pela nave inimiga*/
        forcaRX += calculaForca(nave1.pos_x, nave2.pos_x, nave1.massa, nave2.massa);
        forcaRY += calculaForca(nave1.pos_y, nave2.pos_y, nave1.massa, nave2.massa);
        
        /* força exercida pelos projéteis */
        for(i = 0; i < numeroBalas; i++)
        {
        	forcaRX += calculaForca(nave1.pos_x, balas[i].pos_x, nave1.massa, balas[i].massa);
            forcaRY += calculaForca(nave1.pos_y, balas[i].pos_y, nave1.massa, balas[i].massa);
		}
            
        printf("\nForça resultante em X: %e", forcaRX);
        printf("\nForça resultante em Y: %e", forcaRY);

        acelRX = calculaAceleracao(forcaRX, nave1.massa);
        acelRY = calculaAceleracao(forcaRY, nave1.massa);
        printf("\nAceleração resultante em X: %e", acelRX);
        printf("\nAceleração resultante em Y: %e", acelRY);

        nave1.vel_x = calculaVel(acelRX, nave1.vel_x, delta_t, tempo);
        nave1.vel_y = calculaVel(acelRY, nave1.vel_y, delta_t, tempo);
        printf("\nVelocidade resultante em X: %e", nave1.vel_x);
        printf("\nVelocidade resultante em Y: %e", nave1.vel_y);

        nave1.pos_x = calculaPos(nave1.pos_x, nave1.vel_x, acelRX, delta_t, tempo);
        nave1.pos_y = calculaPos(nave1.pos_y, nave1.vel_y, acelRY, delta_t, tempo);
        printf("\nPosição resultante em X: %e", nave1.pos_x);
        printf("\nPosição resultante em Y: %e\n", nave1.pos_y);

        printf("\n--------------------------------------------------------------");
        printf("\nNAVE 2: ESPECIFICAÇÕES\n");
        
        /* força exercida em B pelo planeta*/
        forcaRX = calculaForca(nave2.pos_x, 0, nave2.massa, planetaP);
        forcaRY = calculaForca(nave2.pos_y, 0, nave2.massa, planetaP);

        /* força exercida em B pela nave A*/
        forcaRX += calculaForca(nave2.pos_x, nave1.pos_x, nave2.massa, nave1.massa);
        forcaRY += calculaForca(nave2.pos_y, nave1.pos_y, nave2.massa, nave1.massa);
        
        /* força exercida pelos projéteis */
        for(i = 0; i < numeroBalas; i++)
        {
        	forcaRX += calculaForca(nave2.pos_x, balas[i].pos_x, nave2.massa, balas[i].massa);
            forcaRY += calculaForca(nave2.pos_y, balas[i].pos_y, nave2.massa, balas[i].massa);
		}
            
        printf("\nForça resultante em X: %e", forcaRX);
        printf("\nForça resultante em Y: %e", forcaRY);

        acelRX = calculaAceleracao(forcaRX, nave2.massa);
        acelRY = calculaAceleracao(forcaRY, nave2.massa);
        printf("\nAceleração resultante em X: %e", acelRX);
        printf("\nAceleração resultante em Y: %e", acelRY);

        nave2.vel_x = calculaVel(acelRX, nave2.vel_x, delta_t, tempo);
        nave2.vel_y = calculaVel(acelRY, nave2.vel_y, delta_t, tempo);
        printf("\nVelocidade resultante em X: %e", nave2.vel_x);
        printf("\nVelocidade resultante em Y: %e", nave2.vel_y);

        nave2.pos_x = calculaPos(nave2.pos_x, nave2.vel_x, acelRX, delta_t, tempo);
        nave2.pos_y = calculaPos(nave2.pos_y, nave2.vel_y, acelRY, delta_t, tempo);
        printf("\nPosição resultante em X: %e", nave2.pos_x);
        printf("\nPosição resultante em Y: %e\n", nave2.pos_y);

        for(i = 0; i < numeroBalas; i++) {
            printf("\n--------------------------------------------------------------");
            printf("\nPROJÉTIL[%d]: ESPECIFICAÇÕES\n", i+1);
                /* força exercida no projétil pelo planeta */
                forcaRX = calculaForca(balas[i].pos_x, 0, balas[i].massa, planetaP);
                forcaRY = calculaForca(balas[i].pos_y, 0, balas[i].massa, planetaP);

                /* força exercida no projétil pela nave A */
                forcaRX += calculaForca(balas[i].pos_x, nave1.pos_x, balas[i].massa, nave1.massa);
                forcaRY += calculaForca(balas[i].pos_y, nave1.pos_y, balas[i].massa, nave1.massa);
                
                /* força exercida no projétil pela nave B */
                forcaRX += calculaForca(balas[i].pos_x, nave2.pos_x, balas[i].massa, nave2.massa);
                forcaRY += calculaForca(balas[i].pos_y, nave2.pos_y, balas[i].massa, nave2.massa);

                /* força exercida pelos projéteis, conta um caso a mais para i == j,
                * mas a força não muda
                */
                for(j = 0; j < numeroBalas; j++) {
                    forcaRX += calculaForca(balas[i].pos_x, balas[j].pos_x, balas[i].massa, balas[j].massa);
                    forcaRY += calculaForca(balas[i].pos_y, balas[j].pos_y, balas[i].massa, balas[j].massa);
                }

                printf("\nForça resultante em X: %e", forcaRX);
                printf("\nForça resultante em Y: %e", forcaRY);

                acelRX = calculaAceleracao(forcaRX, balas[i].massa);
                acelRY = calculaAceleracao(forcaRY, balas[i].massa);
                printf("\nAceleração resultante em X: %e", acelRX);
                printf("\nAceleração resultante em Y: %e", acelRY);

                balas[i].vel_x = calculaVel(acelRX, balas[i].vel_x, delta_t, tempo);
                balas[i].vel_y = calculaVel(acelRY, balas[i].vel_y, delta_t, tempo);
                printf("\nVelocidade resultante em X: %e", balas[i].vel_x);
                printf("\nVelocidade resultante em Y: %e", balas[i].vel_y);

                balas[i].pos_x = calculaPos(balas[i].pos_x, balas[i].vel_x, acelRX, delta_t, tempo);
                balas[i].pos_y = calculaPos(balas[i].pos_y, balas[i].vel_y, acelRY, delta_t, tempo);
                printf("\nPosição resultante em X: %e", balas[i].pos_x);
                printf("\nPosição resultante em Y: %e\n", balas[i].pos_y);
        }
    }
    printf("\n");
}


int main() {

    getParameters();

    return 0;

}

