/**********************************************************************
	MAC0216 - Tecnicas de Programação
	
	Equipe: 
		Daniel Angelo Esteves Lawand
		Lara Ayumi Nagamatsu
		Lucy Anne de Omena Evangelista
		Nathan de Oliveira Nunes

***********************************************************************/

/* Inicia a lista ligada com cabeça das balas */
void initBalas();

/* A partir dos parâmetros da nave, gera-se os dados para uma bala instanciada */
void novaBala(WINDOW *w1, double navePosX, double navePosY, double naveVelX, double naveVelY, int navePos);

/* Retira-se a máscara de todas as balas */
void UnSetMaskBalas();

/* Aplica-se a máscara em todas as balas */
void SetMaskBalas(WINDOW *w1);

cBala * balasTopo();

cBala * freeBala(cBala *balinha);

void freeListaBalas();