/**********************************************************************
	MAC0216 - Tecnicas de Programação
	
	Equipe: 
		Daniel Angelo Esteves Lawand
		Lara Ayumi Nagamatsu
		Lucy Anne de Omena Evangelista
		Nathan de Oliveira Nunes

***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "xwc.h"
#include "parameters.h"

/*-----------------------------------CONSTANTES------------------------------------------*/

#define pi          3.14159265359		/* importante para pegar a direção */
#define G           0.000000000067      /* constante gravitacional universal*/
 
/*---------------------------------------------------------------------------------------*/

/*************spacewars*****************************************
 * double calculaForca()
 * Na função abaixo, o interesse é de calcular as forças 
 * em qualquer eixo sobre o corpo A a partir do corpo B
 ******************************************************/
double calculaForca(double pos_a, double pos_b, double massa_a, double massa_b) {
  
    double forca, delta;

    /* delta é a distância de pos_a a pos_b */
	delta = pos_a - pos_b;
    /*
    if(delta < 0) delta = -delta;*/

    /* cálculo da força, a questão da colisão será refletida em outra parte */
    forca = (G * massa_a * massa_b)/(delta*delta);

    /* 
     * Caso o corpo A esteja à direita do corpo B no eixo X ou
     * caso o corpo A esteja acima do corpo B no eixo Y, a força
     * que B exerce em A "puxa" o corpo A para a esquerda, ou o
     * "puxa" para baixo, de qualquer forma a força é negativa.
     */
    if(pos_a > pos_b) forca = -forca;

    return forca;
}


 /******************************************************
  * double calculaAceleracao()
  *	Função para cálculo da aceleração com base na força
  * e massa do objeto.
  *
 ******************************************************/
double calculaAceleracao(double totalForca, double massa) {
    return (totalForca/massa);
}

/******************************************************
 * double calculaVel()
 *	Função para cálculo da aproximação da velocidade a partir do
 *	Método de Runge Kutta de segunda ordem (Método de Heun)
 *
 * link de referência:/ http://conteudo.icmc.usp.br/pessoas/andretta/ensino/aulas/sme0300-2-13-fisica/aula20-edorungekutta.pdf
 * 
 * ^^^^^
 * Antes, a implementação era a que se encontra acima;
 * Entretanto, alteramos a implementação a partir de
 * um runge-kutta de primeira ordem, chamado Método de 
 * Euler que se baseia no cálculo das derivadas.
 ******************************************************/
double calculaVel(double acel, double vel_0, double passo, double tempototal) {
	/* double k1, k2, runge;
	
	k1 = vel_0 + acel * tempototal; 
    k2 = (vel_0 + passo * k1 * 2./3.) + acel * (tempototal + passo * 2./3.);
    
    runge = vel_0 + (passo/4.) * (k1 + 3*k2);
    
    return runge;*/

    return (vel_0 + passo * acel);
}


 /******************************************************
  * double calculoPos()
  *	Função para cálculo da aproximação da velocidade a partir do
  *	Método de Runge Kutta de segunda ordem (Método de Heun)
  *
  * link de referência:/ http://conteudo.icmc.usp.br/pessoas/andretta/ensino/aulas/sme0300-2-13-fisica/aula20-edorungekutta.pdf
 * ^^^^^
 * Antes, a implementação era a que se encontra acima;
 * Entretanto, alteramos a implementação a partir de
 * um runge-kutta de primeira ordem, chamado Método de 
 * Euler que se baseia no cálculo das derivadas.
 ******************************************************/
double calculaPos(double pos_0, double vel_0, double acel, double passo, double tempototal) {
   /* double pos, k1, k2;
    k1 = pos_0 + vel_0 * tempototal;
    k2 = (pos_0 + passo * k1 * 2./3.) + vel_0 * (tempototal + passo * 2./3.);
    
    pos = pos_0 + (passo/4.) * (k1 + 3*k2);

    return pos;*/
    
    return (pos_0 + passo * vel_0);
} 

/* **********************************************************
 * void getParameters()
 * Função para captação dos parâmetros e exibição de simulação.
 * Por conta do parâmetro da quantidade de balas mudar e assim,
 * consequentemente, as especificações de cada bala poderem ser
 * diferentes, o cálculo das forças exercidas sobre os corpos
 * ocorre na função getParameters.
 ************************************************************/
void getParameters(cBala *ball, cBala *balas, double *tempoDaBala, cNave *nave1, cNave *nave2, double *tempo, double *planetaP, double *planetaR, int *numeroBalas, int *nInt, double *delta_t){
    char nomeArquivo[MAX] = "";
    FILE *entrada;
    int i;
    
    printf("\nPor favor use o arquivo 0.txt ou 1.txt para testar as partes 2 e 3\n");
    printf("%s", "Digite o nome do arquivo de onde carregar os parametros: ");
    /*scanf("%s", nomeArquivo);
    entrada = fopen(nomeArquivo, "r");*/
    entrada = fopen("0.txt", "r");

    
	if (entrada == NULL) 
	{
      printf ("\nNão encontrei o arquivo!\n");
      exit (EXIT_FAILURE);
    }

    /* Primeira Linha do Arquivo: Definições Planetas */
    fscanf(entrada, "%lf ", planetaR); 
    fscanf(entrada, "%lf ", planetaP);
    fscanf(entrada, "%lf ", tempo); 

    /* Definição de delta_t */
    *delta_t = *tempo/(*nInt);

    /* Segunda Linha do Arquivo: nave 1 */
    fflush(stdout);
    fscanf(entrada, "%s ", (nave1->nome));
    fscanf(entrada, "%lf ", &nave1->massa);
    fscanf(entrada, "%lf ", &nave1->pos_x);
    fscanf(entrada, "%lf ", &nave1->pos_y);
    fscanf(entrada, "%lf ", &nave1->vel_x);
    fscanf(entrada, "%lf ", &nave1->vel_y);

    /* Terceira Linha do Arquivo: nave 2 */
    fscanf(entrada, "%s ", (nave2->nome));
    fscanf(entrada, "%lf ", &nave2->massa);
    fscanf(entrada, "%lf ", &nave2->pos_x);
    fscanf(entrada, "%lf ", &nave2->pos_y);
    fscanf(entrada, "%lf ", &nave2->vel_x);
    fscanf(entrada, "%lf ", &nave2->vel_y);

    /* Quarta Linha do Arquivo: numero e tempo das Balas */   
    fscanf(entrada, "%d ", numeroBalas);
    fscanf(entrada, "%lf ", tempoDaBala);

    /* Declarando cada bala */
    
    /* balas = malloc(*numeroBalas * sizeof(cBala)); */
    ball  = (cBala*) realloc(balas, *numeroBalas * sizeof(cBala));

    /* 5+i Linha do Arquivo: iésima bala */
    for (i = 0; i < *numeroBalas; i++) 
	{
	    fscanf(entrada, "%lf ", &(balas[i]).massa);
	    fscanf(entrada, "%lf ", &(balas[i]).pos_x);
	    fscanf(entrada, "%lf ", &(balas[i]).pos_y);
	    fscanf(entrada, "%lf ", &(balas[i]).vel_x);
	    fscanf(entrada, "%lf ", &(balas[i]).vel_y);
    }

    printf("\nplaneta Raio:  %e\n", *planetaR);
    printf("planeta Peso: %e\n", *planetaP);
    printf("tempo: %e\n\n", *tempo);

    printf("Nome nave1: %s\n", nave1->nome);
    printf("Massa nave1: %e\n", nave1->massa);
    printf("Posição x: %e\n", nave1->pos_x);
    printf("Posição y: %e\n", nave1->pos_y);
    printf("Velocidade x: %e\n", nave1->vel_x);
    printf("Velocidade y: %e\n\n", nave1->vel_y);

    printf("Nome nave2: %s\n", nave2->nome);
    printf("Massa nave2: %e\n", nave2->massa);
    printf("Posição x: %e\n", nave2->pos_x);
    printf("Posição y: %e\n", nave2->pos_y);
    printf("Velocidade x: %e\n", nave2->vel_x);
    printf("Velocidade y: %e\n\n", nave2->vel_y);

    printf("Numero Balas: %d\n", *numeroBalas);
    printf("Tempo da Bala: %e\n\n", *tempoDaBala);

    for (i = 0; i < *numeroBalas; i++) 
	{
	    printf("Massa Bala[%d]: %e\n", i+1, balas[i].massa);
	    printf("Vel x Bala[%d]: %e\n", i+1, balas[i].vel_x);
	    printf("Vel y Bala[%d]: %e\n\n", i+1, balas[i].vel_y);
	    printf("Pos x Bala[%d]: %e\n", i+1, balas[i].pos_x);
	    printf("Pos y Bala[%d]: %e\n", i+1, balas[i].pos_y);

    }

}

/* A função teste encontrava-se incorreta até então, pois o cálculo das posições de um corpo acabava sendo
 * alterado antes mesmo do fim do cálculo da posição de outro corpo. A partir disso, a função teste foi
 * alterada e serve apenas como um teste dos arquivos de entrada. 
 */

void teste(double delta_t, double forcaRX, double forcaRY, double acelRX, double acelRY, cNave nave1, cNave nave2, int numeroBalas, double tempo, cBala *balas, int nInt, double planetaP){
    int i;
    int j;
    int ciclo; /* == frames */

    for(ciclo = 0; ciclo < nInt; ciclo++) {
        printf("\n%d CICLO", ciclo+1);
        printf("\n--------------------------------------------------------------");
        printf("\nNAVE 1: ESPECIFICAÇÕES\n");
        /* força exercida em A pelo planeta*/
        forcaRX = calculaForca(nave1.pos_x, 0, nave1.massa, planetaP);
        forcaRY = calculaForca(nave1.pos_y, 0, nave1.massa, planetaP);

        /* força exercida em A pela nave inimiga*/
        forcaRX += calculaForca(nave1.pos_x, nave2.pos_x, nave1.massa, nave2.massa);
        forcaRY += calculaForca(nave1.pos_y, nave2.pos_y, nave1.massa, nave2.massa);
        
        /* força exercida pelos projéteis */
        for(i = 0; i < numeroBalas; i++)
        {
        	forcaRX += calculaForca(nave1.pos_x, balas[i].pos_x, nave1.massa, balas[i].massa);
            forcaRY += calculaForca(nave1.pos_y, balas[i].pos_y, nave1.massa, balas[i].massa);
		}
            
        printf("\nForça resultante em X: %e", forcaRX);
        printf("\nForça resultante em Y: %e", forcaRY);

        acelRX = calculaAceleracao(forcaRX, nave1.massa);
        acelRY = calculaAceleracao(forcaRY, nave1.massa);
        printf("\nAceleração resultante em X: %e", acelRX);
        printf("\nAceleração resultante em Y: %e", acelRY);

        nave1.vel_x = calculaVel(acelRX, nave1.vel_x, delta_t, /*tempo*/ ciclo*delta_t);
        nave1.vel_y = calculaVel(acelRY, nave1.vel_y, delta_t, /*tempo*/ ciclo*delta_t);
        printf("\nVelocidade resultante em X: %e", nave1.vel_x);
        printf("\nVelocidade resultante em Y: %e", nave1.vel_y);


        printf("\n--------------------------------------------------------------");
        printf("\nNAVE 2: ESPECIFICAÇÕES\n");
        
        /* força exercida em B pelo planeta*/
        forcaRX = calculaForca(nave2.pos_x, 0, nave2.massa, planetaP);
        forcaRY = calculaForca(nave2.pos_y, 0, nave2.massa, planetaP);

        /* força exercida em B pela nave A*/
        forcaRX += calculaForca(nave2.pos_x, nave1.pos_x, nave2.massa, nave1.massa);
        forcaRY += calculaForca(nave2.pos_y, nave1.pos_y, nave2.massa, nave1.massa);
        
        /* força exercida pelos projéteis */
        for(i = 0; i < numeroBalas; i++)
        {
        	forcaRX += calculaForca(nave2.pos_x, balas[i].pos_x, nave2.massa, balas[i].massa);
            forcaRY += calculaForca(nave2.pos_y, balas[i].pos_y, nave2.massa, balas[i].massa);
		}
            
        printf("\nForça resultante em X: %e", forcaRX);
        printf("\nForça resultante em Y: %e", forcaRY);

        acelRX = calculaAceleracao(forcaRX, nave2.massa);
        acelRY = calculaAceleracao(forcaRY, nave2.massa);
        printf("\nAceleração resultante em X: %e", acelRX);
        printf("\nAceleração resultante em Y: %e", acelRY);

        nave2.vel_x = calculaVel(acelRX, nave2.vel_x, delta_t, /*tempo*/ciclo*delta_t);
        nave2.vel_y = calculaVel(acelRY, nave2.vel_y, delta_t, /*tempo*/ciclo*delta_t);
        printf("\nVelocidade resultante em X: %e", nave2.vel_x);
        printf("\nVelocidade resultante em Y: %e", nave2.vel_y);


        for(i = 0; i < numeroBalas; i++) {
            printf("\n--------------------------------------------------------------");
            printf("\nPROJÉTIL[%d]: ESPECIFICAÇÕES\n", i+1);
                /* força exercida no projétil pelo planeta */
                forcaRX = calculaForca(balas[i].pos_x, 0, balas[i].massa, planetaP);
                forcaRY = calculaForca(balas[i].pos_y, 0, balas[i].massa, planetaP);

                /* força exercida no projétil pela nave A */
                forcaRX += calculaForca(balas[i].pos_x, nave1.pos_x, balas[i].massa, nave1.massa);
                forcaRY += calculaForca(balas[i].pos_y, nave1.pos_y, balas[i].massa, nave1.massa);
                
                /* força exercida no projétil pela nave B */
                forcaRX += calculaForca(balas[i].pos_x, nave2.pos_x, balas[i].massa, nave2.massa);
                forcaRY += calculaForca(balas[i].pos_y, nave2.pos_y, balas[i].massa, nave2.massa);

                /* força exercida pelos projéteis, conta um caso a mais para i == j,
                * mas a força não muda
                */
                for(j = 0; j < numeroBalas; j++) {
                    forcaRX += calculaForca(balas[i].pos_x, balas[j].pos_x, balas[i].massa, balas[j].massa);
                    forcaRY += calculaForca(balas[i].pos_y, balas[j].pos_y, balas[i].massa, balas[j].massa);
                }

                printf("\nForça resultante em X: %e", forcaRX);
                printf("\nForça resultante em Y: %e", forcaRY);

                acelRX = calculaAceleracao(forcaRX, balas[i].massa);
                acelRY = calculaAceleracao(forcaRY, balas[i].massa);
                printf("\nAceleração resultante em X: %e", acelRX);
                printf("\nAceleração resultante em Y: %e", acelRY);

                balas[i].vel_x = calculaVel(acelRX, balas[i].vel_x, delta_t, tempo);
                balas[i].vel_y = calculaVel(acelRY, balas[i].vel_y, delta_t, tempo);
                printf("\nVelocidade resultante em X: %e", balas[i].vel_x);
                printf("\nVelocidade resultante em Y: %e", balas[i].vel_y);

        }

        printf("Cálculo das posições:\n ");

        /* Posição da NAVE 1 */
        nave1.pos_x = calculaPos(nave1.pos_x, nave1.vel_x, acelRX, delta_t, ciclo*delta_t);
        nave1.pos_y = calculaPos(nave1.pos_y, nave1.vel_y, acelRY, delta_t, ciclo*delta_t);
        printf("\nPosição resultante em X da nave 1: %e", nave1.pos_x);
        printf("\nPosição resultante em Y da nave 1: %e\n", nave1.pos_y);

        /* Posição da NAVE 2 */
        nave2.pos_x = calculaPos(nave2.pos_x, nave2.vel_x, acelRX, delta_t, ciclo*delta_t);
        nave2.pos_y = calculaPos(nave2.pos_y, nave2.vel_y, acelRY, delta_t, ciclo*delta_t);
        printf("\nPosição resultante em X da nave 2: %e", nave2.pos_x);
        printf("\nPosição resultante em Y da nave 2: %e\n", nave2.pos_y);

        /* Posição das balas */
        for( i = 0; i < numeroBalas; i++ ) {
            balas[i].pos_x = calculaPos(balas[i].pos_x, balas[i].vel_x, acelRX, delta_t, ciclo*delta_t);
            balas[i].pos_y = calculaPos(balas[i].pos_y, balas[i].vel_y, acelRY, delta_t, ciclo*delta_t);
            printf("\nPosição resultante em X da bala [%d]: %e\n", i, balas[i].pos_x);
            printf("\nPosição resultante em Y da bala [%d]: %e\n", i, balas[i].pos_y);
        }
    }
    printf("\n");

}

int quasemain() {           /*altere para main caso queira usar a função teste */
    
    double planetaR;        /* define o raio do planeta */
    double planetaP;        /* define massa do planeta */
    double tempo;           /* define tempo total de simulação */
    cNave nave1;
    cNave nave2;
    int numeroBalas;
    double tempoDaBala;
    double delta_t;         /* neste caso, delta_t == tempo/nInt */ 
    int nInt = 200;         /* equivale ao número total de ciclos */
    double forcaRX = 0;
    double forcaRY = 0;
    double acelRX = 0;
    double acelRY = 0;
    cBala *balas;
    cBala *ball;
    getParameters(ball, balas, &tempoDaBala, &nave1, &nave2, &tempo, &planetaP, &planetaR, &numeroBalas, &nInt, &delta_t);
    teste(delta_t, forcaRX, forcaRY, acelRX, acelRY, nave1, nave2, numeroBalas, tempo, balas, nInt, planetaP);
    return 0;

}

void desenhosNave(PIC *des, MASK *masc, WINDOW *w1, char nave[5])
{
    int i;

    

    if (!strcmp(nave, "nave1") /*== 1*/)
    {   /* strcmp retorna 0 para true */
        for(i = 0; i < 16; i++)
        {
            masc[i] = NewMask(w1, 90, 90);
	    }
        des[0] = ReadPic(w1, "merry_1.xpm", masc[0]);
        des[1] = ReadPic(w1, "merry_2.xpm", masc[1]);
        des[2] = ReadPic(w1, "merry_3.xpm", masc[2]);
        des[3] = ReadPic(w1, "merry_4.xpm", masc[3]);
        des[4] = ReadPic(w1, "merry_5.xpm", masc[4]);
        des[5] = ReadPic(w1, "merry_6.xpm", masc[5]);
        des[6] = ReadPic(w1, "merry_7.xpm", masc[6]);
        des[7] = ReadPic(w1, "merry_8.xpm", masc[7]);
        des[8] = ReadPic(w1, "merry_8.xpm", masc[8]);
        des[9] = ReadPic(w1, "merry_10.xpm", masc[9]);
        des[10] = ReadPic(w1, "merry_11.xpm", masc[10]);
        des[11] = ReadPic(w1, "merry_12.xpm", masc[11]);
        des[12] = ReadPic(w1, "merry_13.xpm", masc[12]);
        des[13] = ReadPic(w1, "merry_14.xpm", masc[13]);
        des[14] = ReadPic(w1, "merry_15.xpm", masc[14]);
        des[15] = ReadPic(w1, "merry_16.xpm", masc[15]);
    }
    else /* define a máscara da nave 2*/
    if  (!strcmp(nave, "nave2")) 
    {
        for(i = 0; i < 16; i++)
        {
            masc[i] = NewMask(w1, 90, 90);
	    }
        des[0] = ReadPic(w1, "sunny_1.xpm", masc[0]);
        des[1] = ReadPic(w1, "sunny_2.xpm", masc[1]);
        des[2] = ReadPic(w1, "sunny_3.xpm", masc[2]);
        des[3] = ReadPic(w1, "sunny_4.xpm", masc[3]);
        des[4] = ReadPic(w1, "sunny_5.xpm", masc[4]);
        des[5] = ReadPic(w1, "sunny_6.xpm", masc[5]);
        des[6] = ReadPic(w1, "sunny_7.xpm", masc[6]);
        des[7] = ReadPic(w1, "sunny_8.xpm", masc[7]);
        des[8] = ReadPic(w1, "sunny_8.xpm", masc[8]);
        des[9] = ReadPic(w1, "sunny_10.xpm", masc[9]);
        des[10] = ReadPic(w1, "sunny_11.xpm", masc[10]);
        des[11] = ReadPic(w1, "sunny_12.xpm", masc[11]);
        des[12] = ReadPic(w1, "sunny_13.xpm", masc[12]);
        des[13] = ReadPic(w1, "sunny_14.xpm", masc[13]);
        des[14] = ReadPic(w1, "sunny_15.xpm", masc[14]);
        des[15] = ReadPic(w1, "sunny_16.xpm", masc[15]);
    }
    else
    if  (!strcmp(nave, "mons1"))
    {  
        for(i = 0; i < 3; i++)
        {
            masc[i] = NewMask(w1, 180, 180);
	    }
        
        des[0] = ReadPic(w1, "monstro1.xpm", masc[0]);
        des[1] = ReadPic(w1, "monstro1b.xpm", masc[1]);
        des[2] = ReadPic(w1, "monstro1c.xpm", masc[2]);
    } 
    else
    if ((!strcmp(nave, "mons2")))
    {
        for(i = 0; i < 3; i++)
        {
            masc[i] = NewMask(w1, 180, 180);
	    }
        des[0] = ReadPic(w1, "monstro2.xpm", masc[0]);
        des[1] = ReadPic(w1, "monstro2b.xpm", masc[1]);
        des[2] = ReadPic(w1, "monstro2c.xpm", masc[2]);
    }
    else
    {
        for(i = 0; i < 9; i++)
        {
            masc[i] = NewMask(w1, 170, 170);
	    }
        des[0] = ReadPic(w1, "turbilhao_1.xpm", masc[0]);
        des[1] = ReadPic(w1, "turbilhao_2.xpm", masc[1]);
        des[2] = ReadPic(w1, "turbilhao_3.xpm", masc[2]);
        des[3] = ReadPic(w1, "turbilhao_4.xpm", masc[3]);
        des[4] = ReadPic(w1, "turbilhao_5.xpm", masc[4]);
        des[5] = ReadPic(w1, "turbilhao_6.xpm", masc[5]);
        des[6] = ReadPic(w1, "turbilhao_7.xpm", masc[6]);
        des[7] = ReadPic(w1, "turbilhao_8.xpm", masc[7]);
        des[8] = ReadPic(w1, "turbilhao_9.xpm", masc[8]);
    }
}


int RotacaoRedemoinho (int nCiclos)
{
    return (nCiclos % 8);
}

double anguloBala(int posNave) {

	/* ângulo em graus */
	if (posNave == 0) return 90     *pi/180;
	if (posNave == 1) return 67.5   *pi/180;
	if (posNave == 2) return 45     *pi/180;
	if (posNave == 3) return 22.5   *pi/180;
	if (posNave == 4) return 0      *pi/180;
	if (posNave == 5) return 337.5  *pi/180;
	if (posNave == 6) return 315    *pi/180;
	if (posNave == 7) return 292.5  *pi/180;
	if (posNave == 8) return 270    *pi/180;  
	if (posNave == 9) return 247.5  *pi/180;
	if (posNave == 10) return 225   *pi/180;
	if (posNave == 11) return 202.5 *pi/180;
	if (posNave == 12) return 180   *pi/180;
	if (posNave == 13) return 157.5 *pi/180;
	if (posNave == 14) return 135   *pi/180;
	if (posNave == 15) return 112.5 *pi/180;

    return -1;
}
/*
PIC orientacao(double vel_x, double vel_y, PIC des[16]){
    double seno, cosseno;
    
    if (vel_x == 0 && vel_y == 0)
    {
        seno = 0;
        cosseno = 0;
    }
    else
    {
        seno = sin(vel_y/(sqrt(vel_x * vel_x + vel_y * vel_y)));
        cosseno = cos(vel_x/(sqrt(vel_x * vel_x + vel_y * vel_y)));
    }

    if ((1 >= seno) && (seno > sin(78.75 * pi / 180)))
    {
         return des[0];    
    }
    if ((sin( 78.75 * pi / 180) >= seno) && (seno > sin(56.25 * pi / 180))) 
    {
        if (cosseno >=0)
            return des[1];
        else
            return des[15];
    }
    if ((sin( 56.25 * pi / 180) >=seno) && (seno> sin(33.75 * pi / 180))) 
    { 
        if (cosseno >= 0)
            return des[2];
        else
            return des[14];
    }
    if ((sin( 33.75 * pi / 180) >= seno) && (seno > sin(11.25 * pi / 180))) 
    {
        if (cosseno >=0)
            return des[3];
        else
            return des[13];        
    }

    if ((sin( 11.25 * pi / 180) >= seno) && (seno > sin(348.75 * pi / 180)))
    {
        if (cosseno >=0)
            return des[4];
        else
            return des[12];
    }

    if ((sin( 348.75 * pi / 180) >= seno) && (seno > sin(326.25 * pi / 180))) 
    {
        if (cosseno >=0)
            return des[5];
        else
            return des[11];
    }
    if ((sin( 326.25 * pi / 180) >= seno) && (seno > sin(303.75 * pi / 180))) 
    {
        if (cosseno >=0)
            return des[6];
        else
            return des[10];        
    }
    if ((sin( 303.75 * pi / 180) < seno) && (seno <= sin(281.25 * pi / 180))) 
    {
        if (cosseno >=0)
            return des[7];
        else
            return des[9];
    }
    if ((sin( 281.25 * pi / 180) < seno) && (seno <= -1))
    {
        return des[8];  
    }
}
*/